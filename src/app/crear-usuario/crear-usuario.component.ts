import { Component, OnInit } from '@angular/core';
import { Usuarios } from '../interfaces/Usuarios';
import { UsuariosService } from '../services/usuarios.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuariosComponent implements OnInit {

  Usuario:Usuarios = {
      nombre : null,
      apellido : null,
      correo:null,
      telefono : null,
      direccion : null,
  };

  constructor(private usuario_service:UsuariosService,private _location: Location) { }

  ngOnInit() {
    
  }

  crearUsuario(){
    this.usuario_service.save(this.Usuario).subscribe(response=>{
    });
     setTimeout(()=>this.backClicked(),5000)
    } 
    backClicked() {
        this._location.back();
    }
}

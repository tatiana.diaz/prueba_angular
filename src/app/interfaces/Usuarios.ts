export interface Usuarios{
    id?:number;
    nombre:string;
    apellido:string;
    telefono:string;
    correo:string;
    direccion:string;
}
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuarios } from '../interfaces/Usuarios';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(
    private http:HttpClient
    ) { }

  public getUsuarios(){
    const url ="http://127.0.0.1:8000/api/usuario";
    return this.http.get<Usuarios[]>(url);
  }
  public save(usuarios){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        Authorization: 'my-auth-token'
      })
    };

    const url ="http://127.0.0.1:8000/api/crear_usuario";
    return this.http.post(url,usuarios,httpOptions);
  }
  public edit(usuarios){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        Authorization: 'my-auth-token'
      })
    };
    const url ="http://127.0.0.1:8000/api/editar_usuario";
    return this.http.post(url,usuarios,httpOptions);
  }
  public getUsuariosByID(id){
    const url ="http://127.0.0.1:8000/api/usuario/"+id;
    return this.http.get<Usuarios[]>(url);
  }
  public eliminar(id){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        Authorization: 'my-auth-token'
      })
    };

    const url ="http://127.0.0.1:8000/api/eliminar/"+id;
    return this.http.post(url,httpOptions);
  }
}
